#include<stdio.h>
int main()
{
	int a,b;
	int *p , *q;
	printf("Enter two integers \n");
	scanf("%d%d",&a,&b);
	p=&a;
	q=&b;
	printf("%d + %d = %d \n",*p,*q,*p+*q);
	printf("%d - %d = %d \n",*p,*q,*p-*q);
	printf("%d * %d = %d \n",*p,*q,(*p)*(*q));
	printf("%d / %d = %f \n",*p,*q,(float)*p/(*q));
	printf("%d %% %d = %d \n",*p,*q,*p%*q);
	return 0;
}//write your code here