#include <stdio.h>

int main()
{
    int n,a[n],i,temp;
    printf("Enter the number of array elements \n");
    scanf("%d",&n);
    printf("Enter the elements in ASCENDING ORDER \n");
    for(i=0;i<n;i++){
        scanf("%d",&a[i]);
    }
    printf("Array elements before interchanging: \n");
    for(i=0;i<n;i++){
        printf("%d ",a[i]);
    }
    temp=a[0];
    a[0]=a[n-1];
    a[n-1]=temp;
    printf("\nArray elements after interchanging: \n");
    for(i=0;i<n;i++){
        printf("%d ",a[i]);
    }
    printf("\n");
    return 0;
}
//write your code here