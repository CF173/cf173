#include<stdio.h>
int main()
{
	struct student
	{
		int roll;
		char name[30],section[5],dept[20];
		float fee;
		int result;
	}s1,s2;
	printf("Enter Roll Number of STUDENT 1 \n");
	scanf("%d",&s1.roll);
	printf("Enter Name of STUDENT 1 \n");
	getchar();
	gets(s1.name);
	printf("Enter Section of STUDENT 1 \n");
	scanf("%s",s1.section);
	printf("Enter Department of STUDENT 1 \n");
	getchar();
	gets(s1.dept);
	printf("Enter Fees paid by STUDENT 1 \n");
	scanf("%f",&s1.fee);
	printf("Enter Result(total marks obtained) by STUDENT 1 \n");
	scanf("%d",&s1.result);
	printf("\n\nEnter Roll Number of STUDENT 2 \n");
	scanf("%d",&s2.roll);
	printf("Enter Name of STUDENT 2 \n");
	getchar();
	gets(s2.name);
	printf("Enter Section of STUDENT 2 \n");
	scanf("%s",s2.section);
	printf("Enter Department of STUDENT 2 \n");
	getchar();
	gets(s2.dept);
	printf("Enter Fees paid by STUDENT 2 \n");
	scanf("%f",&s2.fee);
	printf("Enter Result(total marks obtained) by STUDENT 2 \n");
	scanf("%d",&s2.result);
	printf("\n\nDetails of STUDENT with highest score:- \n");
	if(s1.result>s2.result)
		printf("Roll No.: %d\nName: %s\nSection: %s\nDepartment: %s\nFees Paid: %0.3f\nResult: %d\n",s1.roll,s1.name,s1.section,s1.dept,s1.fee,s1.result);
	else
		printf("Roll No.: %d\nName: %s\nSection: %s\nDepartment: %s\nFees Paid: %0.3f\nResult: %d\n",s2.roll,s2.name,s2.section,s2.dept,s2.fee,s2.result);
	return 0;
}