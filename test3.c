#include<stdio.h>
#include<math.h>
int main()
{
  float a,b,c,x1,x2,disc;
  printf("Enter the coefficients of the quadratic equation \n");
  scanf("%f%f%f",&a,&b,&c);
  if(a!=0)
  {
    disc = b*b-4*a*c;
    if(disc == 0)
    {
      printf("The roots are real and equal \n");
	  x1=x2=-b/(2*a);
	  printf("Roots are %f and %f \n",x1,x2);
    }
    else if(disc>0)
    {
      printf("Roots are real and distinct \n");
	  x1=(-b+sqrt(disc))/(2*a);
	  x2=(-b-sqrt(disc))/(2*a);
	  printf("Root 1 = %f \n Root 2 = %f \n ",x1,x2);
    }
    else
    {
      printf("Roots are imaginary \n");
	  x1=-b/(2*a);
	  x2=sqrt(fabs(disc))/(2*a);
	  printf("Root 1 = %f + i%f \n",x1,x2);
	  printf("Root 2 = %f - i%f \n",x1,x2);
     }
  }
  else
    printf("No roots as it is not a quadratic equation \n");
  return 0;
}