#include<stdio.h>
int main()
{
    int n,a[50],i,key,mid,first,last,p=-1;
    printf("Enter the number of array elements \n");
    scanf("%d",&n);
    printf("Enter the elements of array in sorted fashion \n");
    for(i=0;i<n;i++){
        scanf("%d",&a[i]);
    }
    printf("Enter the key element to be searched \n");
    scanf("%d",&key);
    first=0;
    last=n-1;
    while(first<=last){
        mid=(first+last)/2;
        if(key==a[mid]){
            p=mid;
            break;
        }
        else if(a[mid]<key)
            first=mid+1;
        else
            last=mid-1;
    }
    if(p==-1)
        printf("Search unsucessful! Key element not found. \n");
    else
        printf("Search sucessful!! \nKey element %d found at position %d \n",key,p);
    return 0;
}