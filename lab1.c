//write your code here
#include<stdio.h>
#include<math.h>
int main()
{
  float x1,x2,y1,y2;
  float d;
  printf("Enter coordinates of first point \n");
  scanf("%f%f",&x1,&y1);
  printf("Enter coordinates of second point \n");
  scanf("%f%f",&x2,&y2);
  d = sqrt(((x1-x2)*(x1-x2))+((y1-y2)*(y1-y2)));
  printf("Distance between two points = %f \n",d);
  return 0;
}