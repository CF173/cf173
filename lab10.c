#include<stdio.h>
int main(){
	FILE *f;
	char ch;
	f=fopen("INPUT.txt","w");
	printf("Enter the file contents\n");
	while((ch=getchar())!=EOF){
		putc(ch,f);
	}
	fclose(f);
	printf("\nFile contents are:\n");
	f=fopen("INPUT.txt","r");
	while((ch=getc(f))!=EOF){
		printf("%c",ch);
	}
	fclose(f);
	return 0;
}//write your code here